import uuid
from django.db import models
from django.utils.translation import gettext_lazy as _
from django.contrib.auth import get_user_model
from rest_framework.response import Response
from rest_framework.permissions import *
from django.core.exceptions import ObjectDoesNotExist
from rest_framework.response import Response
from locations.serializers.city import *
from locations.models.city import City
from django.core.exceptions import ObjectDoesNotExist
from rest_framework import generics, status, filters
from astorrelay.core.custom_pagination import ResultsSetPagination
from django_filters.rest_framework import DjangoFilterBackend


class CityAddView(generics.ListCreateAPIView):

    queryset = City.objects.all()
    serializer_class = CityAddSerializer
    http_method_names = ["post", "get"]
    filter_backends = [
        filters.SearchFilter,
        DjangoFilterBackend,
        filters.OrderingFilter,
    ]
    filterset_fields = {
        "name": ["exact"],
        "created": ["gte", "lte", "exact", "gt", "lt"],
    }
    search_fields = ["^name"]
    ordering_fields = ["created", "modified"]

    def post(self, request, *args, **kwargs):
        """
        This endpoint lets user add city
        """
        serializer = CityAddSerializer(data=request.data)
        try:
            if not serializer.is_valid():
                data_errors = {}
                data_message = str("")
                for P, M in serializer.errors.items():
                    data_message += P + ": " + M[0].replace(".", "") + ", "
                data_errors["detail"] = data_message
                return Response(data_errors, status=status.HTTP_400_BAD_REQUEST)
            city = City.objects.filter(type_name=request.data["type_name"])
            if city.exists():
                return Response(
                    {"detail": _("A City with the same type_name already exist")},
                    status=status.HTTP_400_BAD_REQUEST,
                )
            city = serializer.save()
            return Response(
                {"detail": _("City successfully added")},
                status=status.HTTP_200_OK,
            )
        except Exception as e:
            return Response({"detail": str(e)}, status=status.HTTP_400_BAD_REQUEST)

    def get(self, request, *args, **kwargs):
        """
        This endpoint retruns a list of cities
        """
        paginator = ResultsSetPagination()
        paginator.page_size = 20
        queryset = self.filter_queryset(self.get_queryset())
        result_page = paginator.paginate_queryset(self.queryset, request)
        serializer = self.serializer_class(result_page, many=True)
        self._paginator = paginator
        return paginator.get_paginated_response(serializer.data)


class CityView(generics.RetrieveUpdateDestroyAPIView):

    queryset = City.objects.all()
    serializer_class = CityEditSerializer
    lookup_field = "pk"
    http_method_names = ["get", "put", "delete"]

    def get(self, request, *args, **kwargs):
        """
        This endpoint displays city details
        """
        try:
            city_viewed = self.queryset.get(id=kwargs["pk"])
            serializer = CityViewSerializer(city_viewed, context={"request": request})
            data = serializer.data
            return Response(data)
        except City.DoesNotExist:
            return Response({"detail": _("This city isn't registered")})

    def put(self, request, *args, **kwargs):
        """
        This endpoint lets user edit city
        """
        try:
            city = self.queryset.get(id=kwargs["pk"])
            partial = kwargs.pop("partial", True)
            serializer = CityEditSerializer(city, data=request.data, partial=partial)
            if not serializer.is_valid():
                data_errors = {}
                data_message = str("")
                for P, M in serializer.errors.items():
                    data_message += P + ": " + M[0].replace(".", "") + ", "
                data_errors["detail"] = data_message
                return Response(data_errors, status=status.HTTP_400_BAD_REQUEST)
            serializer.save()
            return Response(serializer.data)

        except City.DoesNotExist:
            return Response({"detail": _("This city isn't registered")})

    def delete(self, request, *args, **kwargs):
        """
        This endpoint lets user delete city
        """
        try:
            city = self.queryset.get(id=kwargs["pk"])
        except ObjectDoesNotExist:
            return Response(
                {"detail": _("This city does not exist")},
                status=status.HTTP_400_BAD_REQUEST,
            )
        city.delete()
        return Response(
            {"detail": _("City successfully removed")},
            status=status.HTTP_200_OK,
        )
