import uuid
from django.db import models
from django.utils.translation import gettext_lazy as _
from rest_framework.response import Response
from locations.serializers.country import *
from locations.models.country import Country
from rest_framework.permissions import *
from django.core.exceptions import ObjectDoesNotExist
from rest_framework import generics, status
from astorrelay.core.custom_pagination import ResultsSetPagination
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import status, filters


class CountryAddView(generics.ListCreateAPIView):
    queryset = Country.objects.all()
    serializer_class = CountryAddSerializer
    http_method_names = ["post", "get"]
    
    keycloak_roles = {
        'GET': ['admin', 'default-roles-learn']
    }

    filter_backends = [
        filters.SearchFilter,
        DjangoFilterBackend,
        filters.OrderingFilter,
    ]
    filterset_fields = {
        "name": ["exact"],
        "created": ["gte", "lte", "exact", "gt", "lt"],
    }
    search_fields = ["^name"]
    ordering_fields = ["created", "modified"]

    def post(self, request, *args, **kwargs):
        """
        This endpoint lets user add country
        """
        serializer = CountryAddSerializer(data=request.data)
        try:
            if not serializer.is_valid():
                data_errors = {}
                data_message = str("")
                for P, M in serializer.errors.items():
                    data_message += P + ": " + M[0].replace(".", "") + ", "
                data_errors["detail"] = data_message
                return Response(data_errors, status=status.HTTP_400_BAD_REQUEST)
            country = Country.objects.filter(type_name=request.data["type_name"])
            if country.exists():
                return Response(
                    {"detail": _("A Country with the same type_name already exist")},
                    status=status.HTTP_400_BAD_REQUEST,
                )
            country = serializer.save()
            return Response(
                {"detail": _("Country successfully added")},
                status=status.HTTP_200_OK,
            )
        except Exception as e:
            return Response({"detail": str(e)}, status=status.HTTP_400_BAD_REQUEST)

    def get(self, request, *args, **kwargs):
        """
        This endpoint retruns a list of countries
        """
        paginator = ResultsSetPagination()
        paginator.page_size = 20
        queryset = self.filter_queryset(self.get_queryset())
        result_page = paginator.paginate_queryset(self.queryset, request)
        serializer = self.serializer_class(result_page, many=True)
        self._paginator = paginator
        return paginator.get_paginated_response(serializer.data)


class CountryView(generics.RetrieveUpdateDestroyAPIView):
    queryset = Country.objects.all()
    serializer_class = CountryEditSerializer
    lookup_field = "pk"
    http_method_names = ["get", "put", "delete"]

    def get(self, request, *args, **kwargs):
        """
        This endpoint displays country details
        """
        try:
            country_viewed = self.queryset.get(id=kwargs["pk"])
            serializer = CountryViewSerializer(
                country_viewed, context={"request": request}
            )
            data = serializer.data
            return Response(data)
        except Country.DoesNotExist:
            return Response({"detail": _("This country isn't registered")})

    def put(self, request, *args, **kwargs):
        """
        This endpoint lets user edit country
        """
        try:
            country = self.queryset.get(id=kwargs["pk"])
            partial = kwargs.pop("partial", True)
            serializer = CountryEditSerializer(
                country, data=request.data, partial=partial
            )
            if not serializer.is_valid():
                data_errors = {}
                data_message = str("")
                for P, M in serializer.errors.items():
                    data_message += P + ": " + M[0].replace(".", "") + ", "
                data_errors["detail"] = data_message
                return Response(data_errors, status=status.HTTP_400_BAD_REQUEST)
            serializer.save()
            return Response(serializer.data)
        except Country.DoesNotExist:
            return Response({"detail": _("This country isn't registered")})

    def delete(self, request, *args, **kwargs):
        """
        This endpoint lets user delete country
        """
        try:
            country = self.queryset.get(id=kwargs["pk"])
        except ObjectDoesNotExist:
            return Response(
                {"detail": _("This country does not exist")},
                status=status.HTTP_400_BAD_REQUEST,
            )
        country.delete()
        return Response(
            {"detail": _("Country successfully removed")},
            status=status.HTTP_200_OK,
        )
