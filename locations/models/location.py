import uuid
from django.db import models
from django.utils.translation import gettext_lazy as _


class Location(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    address = models.CharField(verbose_name=_("Address"), max_length=150, blank=True)
    lat = models.DecimalField(
        verbose_name=_("Latitude"),
        null=False,
        decimal_places=7,
        max_digits=20,
        default=0,
    )
    lng = models.DecimalField(
        verbose_name=_("Longitude"),
        null=False,
        decimal_places=7,
        max_digits=20,
        default=0,
    )
    created_at = models.DateTimeField(verbose_name=_("Created At"), auto_now_add=True)

    class Meta:
        ordering = ("created_at",)
        verbose_name = _("Location")
        verbose_name_plural = _("Locations")

    def __str__(self):
        return str(self.address)
