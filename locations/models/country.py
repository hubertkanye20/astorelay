import uuid
import os
from slugify import slugify
from django.db import models
from django.utils.translation import gettext_lazy as _


def get_upload_file_flag(instance, filename):
    ext = filename.split(".")[-1]
    filename = "%s.%s" % (slugify(instance.iso_2), ext)
    return os.path.join("countries/flags/", filename)


class Country(models.Model):
    id = models.UUIDField(
        primary_key=True, default=uuid.uuid4, unique=True, editable=False
    )
    name = models.CharField(max_length=100, unique=True)
    capital_city = models.CharField(max_length=100, blank=True, null=True)
    country_code = models.CharField(max_length=50, blank=True, null=True)
    continent = models.CharField(max_length=100, blank=True, null=True)
    currency_short_name = models.CharField(max_length=50, blank=True, null=True)
    currency_full_name = models.CharField(max_length=50, blank=True, null=True)
    currency_code = models.CharField(max_length=100, blank=True, null=True)
    currency_symbol = models.CharField(max_length=3, blank=True, null=True)
    iso_2 = models.CharField(max_length=2, blank=True, null=True)
    iso_3 = models.CharField(max_length=3, blank=True, null=True)
    latitude = models.FloatField(default=0.0)
    longitude = models.FloatField(default=0.0)
    flag = models.FileField(upload_to=get_upload_file_flag, blank=True, null=True)
    population = models.IntegerField(default=0)
    timezone = models.CharField(max_length=20, blank=True, null=True)
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)
    is_active = models.BooleanField(default=True)

    class Meta:
        ordering = ("created",)
        verbose_name = _("Country")
        verbose_name_plural = _("Countries")

    def __str__(self):
        return self.name
