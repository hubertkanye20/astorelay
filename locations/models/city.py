import uuid
from django.db import models
from locations.models.province import Province
from django.utils.translation import gettext_lazy as _


class City(models.Model):
    id = models.UUIDField(
        primary_key=True, default=uuid.uuid4, unique=True, editable=False
    )
    name = models.CharField(max_length=200)
    state_province_id = models.ForeignKey(
        Province,
        null=True,
        blank=True,
        related_name="cities",
        on_delete=models.CASCADE,
    )
    latitude = models.FloatField(default=0.0)
    longitude = models.FloatField(default=0.0)
    created = models.DateTimeField(auto_now=True)
    modified = models.DateTimeField(auto_now_add=True)
    is_active = models.BooleanField(default=True)

    class Meta:
        ordering = ("created",)
        verbose_name = _("City")
        verbose_name_plural = _("Cities")

    def __str__(self):
        return self.name
