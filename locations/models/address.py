import uuid
from django.db import models
from locations.models.country import Country
from locations.models.province import Province
from locations.models.city import City
from django.utils.translation import gettext_lazy as _


class Address(models.Model):
    """
    Link: https://github.com/cvanderwall14/django-address/blob/master/address/models.py
    .address = {
            "raw": "Volodymyrska st, 10",
            "country": "Ukraine",  # can use pk or instance country model
            "country_code": "UA",
            "region": "Kyiv City",  # can use pk or instance Region model
            "region_code": "UA-32",
            "district": "",  # can use pk or instance District model
            "district_code": "",
            "locality": "Kiev",
            "street": "Volodymyrska street",
            "street_number": "10",
            "postal_code": "02000",
            "latitude": 50.456302,
            "longitude": 30.517044,
            "formatted_address": "Khreschatyk St, 15, Kyiv, Ukraine, 02000",
        }
    """

    id = models.UUIDField(
        primary_key=True, default=uuid.uuid4, unique=True, editable=False
    )
    address = models.CharField(
        verbose_name=_("Address"), max_length=255, blank=True, null=True
    )
    street_number = models.CharField(
        verbose_name=_("Street Number"), max_length=50, blank=True, null=True
    )
    street = models.CharField(
        verbose_name=_("Street"), max_length=100, blank=True, null=True
    )
    postal_code = models.CharField(
        verbose_name=_("Postal Code"), max_length=100, blank=True, null=True
    )
    city_id = models.ForeignKey(
        City,
        blank=True,
        null=True,
        related_name="zone_cities",
        on_delete=models.CASCADE,
    )
    raw = models.CharField(max_length=1000, blank=True, null=True)
    province_id = models.ForeignKey(
        Province,
        blank=True,
        null=True,
        related_name="zone_provinces",
        on_delete=models.CASCADE,
    )
    country_id = models.ForeignKey(
        Country,
        blank=True,
        null=True,
        related_name="zone_countries",
        on_delete=models.CASCADE,
    )
    latitude = models.FloatField(verbose_name=_("Latitude"), blank=True, null=True)
    longitude = models.FloatField(verbose_name=_("Longitude"), blank=True, null=True)
    created_at = models.DateTimeField(auto_now=True)

    class Meta:
        ordering = ("created_at",)
        verbose_name = _("Address")
        verbose_name_plural = _("Addresses")

    def __str__(self):
        return self.address

    def __str__(self):
        txt = ""
        if self.street_number:
            txt = self.street_number
        if self.street:
            if txt:
                txt += " "
            txt += self.street
        if self.city:
            if txt:
                txt += ", "
            txt += str(self.city_id)
        if self.state:
            if txt:
                txt += ", "
            txt += str(self.province_id)
        if self.postal_code:
            if txt:
                txt += " "
            txt += str(self.postal_code)
        if self.country:
            if txt:
                txt += ", "
            txt += str(self.country_id)
        if txt:
            return txt
        else:
            return self.raw

    def save(self, *args, **kwargs):
        gmaps = googlemaps.Client(key=local_settings.GOOGLE_MAPS_KEY)
        geocode_result = gmaps.geocode(str(self))
        self.latitude = geocode_result[0]["geometry"]["location"]["lat"]
        self.longitude = geocode_result[0]["geometry"]["location"]["lng"]
        super(Address, self).save(*args, **kwargs)
