import uuid
from django.db import models
from locations.models.country import Country
from django.utils.translation import gettext_lazy as _


class Province(models.Model):
    id = models.UUIDField(
        primary_key=True, default=uuid.uuid4, unique=True, editable=False
    )
    name = models.CharField(max_length=200)
    country_id = models.ForeignKey(
        Country,
        related_name="provinces",
        on_delete=models.CASCADE,
    )
    zip_code = models.CharField(max_length=10, blank=True)
    latitude = models.FloatField(default=0.0)
    longitude = models.FloatField(default=0.0)
    created = models.DateTimeField(auto_now=True)
    modified = models.DateTimeField(auto_now_add=True)
    is_active = models.BooleanField(default=True)

    class Meta:
        ordering = ("created",)
        verbose_name = _("Province")
        verbose_name_plural = _("Provinces")

    def __str__(self):
        return self.name
