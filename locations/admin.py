from django.contrib import admin
from locations.models.country import Country
from locations.models.province import Province
from locations.models.city import City
from locations.models.address import Address
from locations.models.location import Location


class CountryAdmin(admin.ModelAdmin):
    list_display = (
        "id",
        "name",
        "country_code",
        "iso_2",
        "iso_3",
    )
    search_fields = ["name"]
    list_per_page = 50
    list_filter = [
        "country_code",
        "iso_2",
        "iso_3",
    ]


class ProvinceAdmin(admin.ModelAdmin):
    list_display = ("id", "name", "country_id")
    search_fields = ["name"]
    list_per_page = 50
    list_filter = ["latitude", "longitude"]


class CityAdmin(admin.ModelAdmin):
    list_display = ("name",)
    search_fields = ["name"]
    list_per_page = 50
    list_filter = ["latitude", "longitude"]


class AddressAdmin(admin.ModelAdmin):
    list_display = (
        "address",
        "street",
        "street_number",
    )
    search_fields = ["address"]
    list_per_page = 50
    list_filter = ["created_at", "latitude", "longitude"]


class LocationAdmin(admin.ModelAdmin):
    list_display = ("address", "lat", "lng")
    search_fields = ["address"]
    list_per_page = 50
    list_filter = ["lat", "lng"]


admin.site.register(Country, CountryAdmin)
admin.site.register(Province, ProvinceAdmin)
admin.site.register(City, CityAdmin)
admin.site.register(Address, AddressAdmin)
admin.site.register(Location, LocationAdmin)
