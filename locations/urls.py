from django.urls import path, re_path
from django.urls import path
from locations.views import country, city


urlpatterns = [
   
    # Country
    path("countries/", country.CountryAddView.as_view(), name="country_add"),
    re_path(
        r"^country/(?P<pk>[a-z0-9\-]+)/$",
        country.CountryView.as_view(),
        name="country_view",
    ),
    # City
    path("cities/", city.CityAddView.as_view(), name="city_add"),
    re_path(
        r"^city/(?P<pk>[a-z0-9\-]+)/$",
        city.CityView.as_view(),
        name="city_view",
    ),
]
