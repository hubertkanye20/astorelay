from modeltranslation.translator import translator, TranslationOptions
from locations.models.country import Country
from locations.models.city import City


class CountryTranslationOptions(TranslationOptions):

    # These are the field to be translated
    fields = [
        "name",
    ]


translator.register(Country, CountryTranslationOptions)


class CityTranslationOptions(TranslationOptions):

    # These are the field to be translated
    fields = ["name"]


translator.register(City, CityTranslationOptions)
