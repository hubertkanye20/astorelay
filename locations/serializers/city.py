from rest_framework import serializers
from locations.models.city import City


class CityAddSerializer(serializers.ModelSerializer):
    class Meta:
        model = City
        fields = [
            "id",
            "name",
        ]
        extra_kwargs = {
            "modified_by": {"read_only": True},
            "created_by": {"read_only": True},
            "created": {"read_only": True},
            "modified": {"read_only": True},
        }


class CitySerializer(serializers.ModelSerializer):
    class Meta:
        model = City
        fields = [
            "id",
            "name",
        ]
        extra_kwargs = {
            "modified_by": {"read_only": True},
            "created_by": {"read_only": True},
            "created": {"read_only": True},
            "modified": {"read_only": True},
        }


class CityEditSerializer(serializers.ModelSerializer):
    class Meta:
        model = City
        fields = [
            "id",
            "name",
        ]
        extra_kwargs = {
            "modified_by": {"read_only": True},
            "created_by": {"read_only": True},
            "created": {"read_only": True},
            "modified": {"read_only": True},
        }


class CityViewSerializer(serializers.ModelSerializer):
    class Meta:
        model = City
        fields = [
            "id",
            "name",
        ]
        extra_kwargs = {
            "modified_by": {"read_only": True},
            "created_by": {"read_only": True},
            "created": {"read_only": True},
            "modified": {"read_only": True},
        }


class CityListSerializer(serializers.ModelSerializer):
    class Meta:
        model = City
        fields = [
            "id",
            "name",
        ]
        extra_kwargs = {
            "modified_by": {"read_only": True},
            "created_by": {"read_only": True},
            "created": {"read_only": True},
            "modified": {"read_only": True},
        }
