from rest_framework import serializers
from locations.models.country import Country


class CountryAddSerializer(serializers.ModelSerializer):
    class Meta:
        model = Country
        fields = [
            "id",
            "name",
        ]
        extra_kwargs = {
            "modified_by": {"read_only": True},
            "created_by": {"read_only": True},
            "created": {"read_only": True},
            "modified": {"read_only": True},
        }


class CountrySerializer(serializers.ModelSerializer):
    class Meta:
        model = Country
        fields = [
            "id",
            "name",
        ]
        extra_kwargs = {
            "modified_by": {"read_only": True},
            "created_by": {"read_only": True},
            "created": {"read_only": True},
            "modified": {"read_only": True},
        }


class CountryEditSerializer(serializers.ModelSerializer):
    class Meta:
        model = Country
        fields = [
            "id",
            "name",
        ]
        extra_kwargs = {
            "modified_by": {"read_only": True},
            "created_by": {"read_only": True},
            "created": {"read_only": True},
            "modified": {"read_only": True},
        }


class CountryViewSerializer(serializers.ModelSerializer):
    class Meta:
        model = Country
        fields = [
            "id",
            "name",
        ]
        extra_kwargs = {
            "modified_by": {"read_only": True},
            "created_by": {"read_only": True},
            "created": {"read_only": True},
            "modified": {"read_only": True},
        }


class CountryListSerializer(serializers.ModelSerializer):
    class Meta:
        model = Country
        fields = [
            "id",
            "name",
        ]
        extra_kwargs = {
            "modified_by": {"read_only": True},
            "created_by": {"read_only": True},
            "created": {"read_only": True},
            "modified": {"read_only": True},
        }
