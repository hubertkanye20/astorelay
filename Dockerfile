# Dockerfile
# Pull base image
FROM python:3.8

# Set environment variables
ENV VIRTUAL_ENV=/opt/venv
RUN python3 -m venv $VIRTUAL_ENV
ENV PATH="$VIRTUAL_ENV/bin:$PATH"
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

RUN apt-get update
RUN apt-get install -y build-essential
RUN apt-get install -y python3-dev
RUN apt-get install -y swig
RUN apt-get install -y gettext
RUN apt-get install -y libgettextpo-dev
RUN apt-get install -y graphviz-dev
# RUN apt-get install python-openstackclient python-novaclient -y
ENV DJANGO_SETTINGS_MODULE=astorrelay.settings.development


# Set work directory
RUN mkdir /code/
WORKDIR /code/

# install dependencies
COPY requirements.txt /code
COPY openrc.sh /code
RUN pip install --upgrade pip
RUN pip install -r requirements.txt
RUN /bin/bash -c "source openrc.sh"
# Copy project
COPY . /code/

