from django.contrib import admin
from django.conf import settings
from drf_yasg import openapi
from rest_framework import permissions
from drf_yasg.views import get_schema_view
from django.urls import path, re_path, include
from django.views.generic import TemplateView
from django.conf.urls.i18n import i18n_patterns

# from permissions.views import group
from django.conf.urls.static import static


from companies.views import (
    legal_form,
)


schema_view = get_schema_view(
    openapi.Info(
        title="AstorRelay API",
        default_version="v1",
        description="Parcel shipping System",
        terms_of_service="https://www.astorprotect.com/policies/terms/",
        contact=openapi.Contact(email="contact@astorprotect.com"),
        license=openapi.License(name="AstorRelay License"),
    ),
    public=True,
    permission_classes=(permissions.AllowAny,),
)

urlpatterns = [
    path("admin/", admin.site.urls),
    path("companies/", include("companies.urls")),
    # path("payments/", include("payments.urls")),
    path("locations/", include("locations.urls")),
    path("authentication/", include("rest_framework.urls", namespace="rest_framework")),
    re_path(
        r"^swagger(?P<format>\.json|\.yaml)$",
        schema_view.without_ui(cache_timeout=0),
        name="schema-json",
    ),
    re_path(
        r"^swagger/$",
        schema_view.with_ui("swagger", cache_timeout=0),
        name="schema-swagger-ui",
    ),
    re_path(
        r"^redoc/$", schema_view.with_ui("redoc", cache_timeout=0), name="schema-redoc"
    ),
 
    # Legal form
    path("legalforms/", legal_form.LegalFormAddView.as_view(), name="legal_form_add"),
    re_path(
        r"^legalforms/(?P<pk>[a-z0-9\-]+)/$",
        legal_form.LegalFormView.as_view(),
        name="legal_form_view",
    ),

]


if getattr(settings, "REST_USE_JWT", False):
    from rest_framework_simplejwt.views import TokenVerifyView
    from dj_rest_auth.jwt_auth import get_refresh_view

    urlpatterns += [
        path("token/verify/", TokenVerifyView.as_view(), name="token_verify"),
        path("token/refresh/", get_refresh_view().as_view(), name="token_refresh"),
    ]

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)


admin.site.site_header = "Astor Relay Admin"
admin.site.site_title = "Astor Relay Admin Portal"
admin.site.index_title = "Welcome to Astor Relay API Portal"
