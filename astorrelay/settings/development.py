# Easycook/settings/development.py
from .base import *
from .search_index import *

# Set Debug True only in dev and preprod

SECRET_KEY = "$8l^bv34-v-olti&53$jkax&2@br4lk@^7+z4z9j#*xwh!ifwx"

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

# ALLOWED_HOSTS = [".wadpark.com"]
API_BASE_URL = "https://api.sandbox.astorrelay.com"

# Only allow this in dev
ALLOWED_HOSTS = ["*"]
