# Name of the Elasticsearch index
ELASTICSEARCH_INDEX_NAMES = {
    "searches.documents.parking_lot": "parking_lot",
    "searches.documents.language": "language",
    "searches.documents.country": "country",
    "searches.documents.legal_form": "legal_form",
}
