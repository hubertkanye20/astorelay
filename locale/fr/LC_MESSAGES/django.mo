��             +         �     �     �     �     �     �               -  
   5     @  *   G     r  
   �  	   �     �     �     �  �   �     G  .   d  $   �  /   �  U   �     >  %   W  6   }  4   �  &   �       2   *  *   ]  A  �     �     �     �     �     �  "   
     -     <     D  	   L  +   V     �     �     �     �     �     �  �   �  ,   �	  &   �	  ?   �	  M   !
  Z   o
  %   �
  /   �
  =      =   ^  8   �  (   �  ?   �  +   >                   
                                                                                                 	                            Active An error has occurred. Client Clients Date joined Define adapter_class in view Email address English First name French Incorrect input. access_token is required. Incorrect value Last login Last name Last updated Licence plate Modified Neither cookies or blacklist are enabled, so the token has not been deleted server side. Please make sure the token is deleted client side. New password has been saved. Password has been reset with the new password. Password reset e-mail has been sent. Refresh token was not included in request data. Registration successfully completed. Please check your email to activate your account Successfully logged out. The two password fields didn't match. There is no account registered with your email address User is already registered with this e-mail address. Users must have a valid email address. Verification e-mail sent. View is not defined, pass it as a context variable You have successfully confirmed your Email Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
 Activé Une erreur est survenue Client Clients Date d'adhésion Définir adapter_class dans la vue Adresse e-mail Anglais Prénom Français Entrée incorrecte. access_token est requis Valeur incorrecte Dernière connexion Nom Dernière mise à jour Plaque d'immatriculation Modifié Ni les cookies ni la liste noire ne sont activés, le jeton n'a donc pas été supprimé.côté serveur. Veuillez vous assurer que le jeton est supprimé côté client Le nouveau mot de passe a été enregistré. Le mot de passe a été réinitialisé L’email de réinitialisation du mot de passe a été envoyé. Le jeton d'actualisation n'était pas inclus dans les données de la demande. Inscription passée avec succès. Veuillez vérifier votre email pour activer votre compte Déconnexion effectuée avec succès. Les deux mots de passes ne sont pas les mêmes. Il n'y a pas de compte enregistré avec votre adresse e-mail. Utilisateur est déjà enregistré avec cette adresse e-mail. Les utilisateurs doivent avoir une adresse e-mail valide L'email de vérification a été envoyé La vue n'est pas définie, passez-la comme variable de contexte Vous avez confirmé avec succès votre Emai 