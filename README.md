# astorrelay API 

Documentation on how to use the Authentication Module

## User Registration?

### for Client:

Check out the [post](http://localhost:8010/accounts/signup/client/).

### for Partner:

Check out the [post](http://localhost:8010/accounts/signup/partner/).

### for Controller:

Check out the [post](http://localhost:8010/accounts/signup/controller/).


After successful registration, a confirmation email containing a key is sent to the user. That key will then be used to confirm his account.
Example of the link sent to the user: 

```
http://astorrelay.com/en/account-confirm-email/MTA:1krR93:gCnPU5VxUGs0Mk9qxxmsX9OzRH1BTGsz5ZUg9U5HxaE/
```
## User Email Confirmation/Activation?

Grab the key received above and post it in the following endpoint:

Check out the [post](http://localhost:8010/accounts/email/verification/).
After successful confirmation, the user's account will be automatically activated.

## User Password Reset?

Check out the [post](http://localhost:8010/accounts/password/reset/).
To reset user's password. After submission, a confirmation email is sent to user within a link containing a token and uuid that will be used to confirm his password reset request.

Example of the link sent to the user: 

```
http://astorrelay.com/en/auth/password/reset/confirm/MjlkNjg1ZWEtNjllOS00NjA2LWEzYWMtY2ZiNjEzOWIyNjcy/af8y7j-4c94b348e450b632ab9a6e95da86c941/
```
## User Password Reset Email Confirmation?

Grab the token and uiid received above and post it in the following endpoint:

Check out the [post](http://localhost:8010/accounts/password/reset/confirm/).
After successful confirmation, the user's password is reset.

## User Password Change?

Check out the [post](http://localhost:8010/accounts/password/change/), to change user's password.

## User Login?

Check out the [post](http://localhost:8010/auth/login/?client=N9KXq6OxzZuvpRhqxWuWFzPvf6qmy5Q06KHQiW5R).

After successful login, an access_token, refresh_token are returned back as a response. The client represents Oauth2 Client Application


## User Logout?

Check out the [post](http://localhost:8010/auth/logout/?client=N9KXq6OxzZuvpRhqxWuWFzPvf6qmy5Q06KHQiW5R).

The connected User will be disconnected from the given application

## Google Social Authentication?

#1. The backend receives an access token from the Frontend apps, and that token is used to get user's google data from  Firebase.
    
Check out the [post](http://localhost:8010/en/auth/google/?client=N9KXq6OxzZuvpRhqxWuWFzPvf6qmy5Q06KHQiW5R).

##################################
# TODO:

1. Add authentication with phone Number
2. Phone Validation
3. Differencier le compte Client Partenaire et Entreprise Partenaire (avec le user_type)
4. Authentification via AppleID avec Firebase
## To run test locally, 

docker-compose run -e DJANGO_SETTINGS_MODULE=astorrelay.settings.testing --no-deps --rm astorpark_web python manage.py migrate --run-syncdb;docker-compose run -e DJANGO_SETTINGS_MODULE=astorrelay.settings.testing --no-deps --rm astorpark_web python manage.py tests/test;


docker-compose run --rm astorrelay sh -c "python manage.py test"


Generates the SQL code for yuou models.
sudo docker-compose run django_recipe_api sh -c "python manage.py makemigrations" 
# Runs the SQL commands that you generated.
sudo docker-compose run django_recipe_api sh -c "python manage.py migrate" 