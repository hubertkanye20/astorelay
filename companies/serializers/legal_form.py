from rest_framework import serializers
from rest_framework.relations import PrimaryKeyRelatedField
from companies.models.legal_form import LegalForm


class LegalFormAddSerializer(serializers.ModelSerializer):
    class Meta:
        model = LegalForm
        fields = [
            "id",
            "name",
        ]
        extra_kwargs = {
            "modified_by": {"read_only": True},
            "created_by": {"read_only": True},
            "created": {"read_only": True},
            "modified": {"read_only": True},
        }


class LegalFormSerializer(serializers.ModelSerializer):
    class Meta:
        model = LegalForm
        fields = [
            "id",
            "name",
        ]
        extra_kwargs = {
            "modified_by": {"read_only": True},
            "created_by": {"read_only": True},
            "created": {"read_only": True},
            "modified": {"read_only": True},
        }


class LegalFormEditSerializer(serializers.ModelSerializer):
    class Meta:
        model = LegalForm
        fields = [
            "id",
            "name",
        ]
        extra_kwargs = {
            "modified_by": {"read_only": True},
            "created_by": {"read_only": True},
            "created": {"read_only": True},
            "modified": {"read_only": True},
        }


class LegalFormViewSerializer(serializers.ModelSerializer):
    class Meta:
        model = LegalForm
        fields = [
            "id",
            "name",
        ]
        extra_kwargs = {
            "modified_by": {"read_only": True},
            "created_by": {"read_only": True},
            "created": {"read_only": True},
            "modified": {"read_only": True},
        }


class LegalFormListSerializer(serializers.ModelSerializer):
    class Meta:
        model = LegalForm
        fields = [
            "id",
            "name",
        ]
        extra_kwargs = {
            "modified_by": {"read_only": True},
            "created_by": {"read_only": True},
            "created": {"read_only": True},
            "modified": {"read_only": True},
        }
