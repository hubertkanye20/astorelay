from rest_framework import serializers
from rest_framework.relations import PrimaryKeyRelatedField
from companies.models.company import Company
from companies.models.document import Document


class CompanyAddSerializer(serializers.ModelSerializer):
    class Meta:
        model = Company
        fields = "__all__"
        # fields = [
        #     "id",
        #     "sponsor_name",
        # ]
        extra_kwargs = {
            # "modified_by": {"read_only": True},
            "user": {"read_only": True},
            "created_at": {"read_only": True},
            "modified_at": {"read_only": True},
        }


class CompanySerializer(serializers.ModelSerializer):
    class Meta:
        model = Company
        fields = [
            "id",
            "company_name",
        ]
        extra_kwargs = {
            "user": {"read_only": True},
            "created_at": {"read_only": True},
            "modified_at": {"read_only": True},
        }


class CompanyEditSerializer(serializers.ModelSerializer):
    class Meta:
        model = Company
        fields = ["id", "company_name", "sponsor_name", "phone", "activity", "address"]
        extra_kwargs = {
            "created_at": {"read_only": True},
            "modified_at": {"read_only": True},
        }


class CompanyViewSerializer(serializers.ModelSerializer):
    class Meta:
        model = Company
        fields = "__all__"
        # fields = [
        #     "id",
        #     "company_name",
        # ]
        # extra_kwargs = {
        #     "modified_by": {"read_only": True},
        #     "created_by": {"read_only": True},
        #     "created": {"read_only": True},
        #     "modified": {"read_only": True},
        # }


class CompanyListSerializer(serializers.ModelSerializer):
    class Meta:
        model = Company
        fields = [
            "id",
            "company_name",
        ]
        extra_kwargs = {
            "created_at": {"read_only": True},
            "modified_at": {"read_only": True},
        }


class OfficialDocumentSerializer(serializers.ModelSerializer):
    class Meta:
        model = Document
        fields = ("document",)
