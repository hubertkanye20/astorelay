from django.conf import settings
from django.contrib.auth import get_user_model
from django.utils import timezone
from django.utils.translation import gettext_lazy as _
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.response import Response
from rest_framework import generics, status, filters
from companies.serializers.company import (
    CompanyViewSerializer,
    CompanyAddSerializer,
    CompanyEditSerializer,
)
from astorrelay.core.custom_pagination import ResultsSetPagination
from django_filters.rest_framework import DjangoFilterBackend
from companies.models.company import Company
from django.core.exceptions import ObjectDoesNotExist
from django.conf import settings
from django.contrib.auth import get_user_model
from django.utils import timezone
from django.utils.translation import gettext_lazy as _
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework import generics, status, filters
from companies.models.document import Document
from rest_framework.generics import get_object_or_404
from astorrelay.core.custom_pagination import ResultsSetPagination
from django_filters.rest_framework import DjangoFilterBackend


class CompanyDetailsView(generics.RetrieveAPIView):
    serializer_class = CompanyViewSerializer
    permission_classes = (IsAuthenticated,)
    queryset = Company.objects.all()
    http_method_names = ["get"]

    def get(self, request, *args, **kwargs):
        """
        This endpoint displays company details
        """
        try:
            company_viewed = self.queryset.get(id=kwargs["pk"])
            serializer = CompanyViewSerializer(
                company_viewed, context={"request": request}
            )
            data = serializer.data
            return Response(data)
        except Company.DoesNotExist:
            return Response({"detail": _("This company isn't registered")})


class CompanyAddView(generics.ListCreateAPIView):
    queryset = Company.objects.all()
    serializer_class = CompanyAddSerializer
    http_method_names = ["get", "post"]
    filter_backends = [
        filters.SearchFilter,
        DjangoFilterBackend,
        filters.OrderingFilter,
    ]
    filterset_fields = {
        "company_name": ["exact"],
        "sponsor_name": ["exact"],
        "created_at": ["gte", "lte", "exact", "gt", "lt"],
    }
    search_fields = ["^company_name", "^sponsor_name"]
    ordering_fields = ["created_at", "modified"]

    def post(self, request, *args, **kwargs):
        """
        This endpoint lets user add language
        """
        serializer = CompanyAddSerializer(data=request.data)
        try:
            if not serializer.is_valid():
                data_errors = {}
                data_message = str("")
                for P, M in serializer.errors.items():
                    data_message += P + ": " + M[0].replace(".", "") + ", "
                data_errors["detail"] = data_message
                return Response(data_errors, status=status.HTTP_400_BAD_REQUEST)
            language = Company.objects.filter(company_name=request.data["company_name"])
            if language.exists():
                return Response(
                    {"detail": _("A Company with the same company_name already exist")},
                    status=status.HTTP_400_BAD_REQUEST,
                )
            language = serializer.save(user=request.user)
            return Response(
                {"detail": _("Company successfully added")},
                status=status.HTTP_200_OK,
            )
        except Exception as e:
            return Response({"detail": str(e)}, status=status.HTTP_400_BAD_REQUEST)

    def get(self, request, *args, **kwargs):
        """
        This endpoint retruns a list of registered companies
        """
        paginator = ResultsSetPagination()
        paginator.page_size = 20
        self.queryset = self.filter_queryset(self.get_queryset())
        result_page = paginator.paginate_queryset(self.queryset, request)
        serializer = self.serializer_class(result_page, many=True)
        self._paginator = paginator
        return paginator.get_paginated_response(serializer.data)


class CompanyView(generics.RetrieveUpdateDestroyAPIView):
    queryset = Company.objects.all()
    serializer_class = CompanyEditSerializer
    lookup_field = "pk"
    http_method_names = ["get", "put", "delete"]

    def get(self, request, *args, **kwargs):
        """
        This endpoint displays company details
        """
        try:
            company_viewed = self.queryset.get(id=kwargs["pk"])
            serializer = CompanyViewSerializer(
                company_viewed, context={"request": request}
            )
            data = serializer.data
            return Response(data)
        except Company.DoesNotExist:
            return Response({"detail": _("This company isn't registered")})

    def put(self, request, *args, **kwargs):
        """
        This endpoint lets user edit company
        """
        try:
            company = self.queryset.get(id=kwargs["pk"])
            partial = kwargs.pop("partial", True)
            serializer = CompanyEditSerializer(
                company, data=request.data, partial=partial
            )
            if not serializer.is_valid():
                data_errors = {}
                data_message = str("")
                for P, M in serializer.errors.items():
                    data_message += P + ": " + M[0].replace(".", "") + ", "
                data_errors["detail"] = data_message
                return Response(data_errors, status=status.HTTP_400_BAD_REQUEST)
            serializer.save()
            return Response(serializer.data)
        except Company.DoesNotExist:
            return Response({"detail": _("This company isn't registered")})

    def delete(self, request, *args, **kwargs):
        """
        This endpoint lets user delete company
        """
        try:
            company = self.queryset.get(id=kwargs["pk"])
        except ObjectDoesNotExist:
            return Response(
                {"detail": _("This company does not exist")},
                status=status.HTTP_400_BAD_REQUEST,
            )
        company.delete()
        return Response(
            {"detail": _("Company successfully removed")},
            status=status.HTTP_200_OK,
        )


# class ScannedDocumentView(generics.CreateAPIView):
#     permission_classes = (IsAuthenticated,)
#     queryset = Document.objects.all()
#     serializer_class = ScannedDocumentSerializer
#     http_method_names = [
#         "post",
#     ]

#     def post(self, request, *args, **kwargs):
#         """
#         This Endpoint is used to add contents. A content can be associated to
#         many other contents via recursive relation. The content_id is appended in
#         the url to add others contents to it!
#         """
#         try:
#             get_user_model().objects.get(email=request.user.email)
#             current_user = get_object_or_404(get_user_model(), email=request.user.email)
#         except get_user_model().DoesNotExist:
#             return Response(status=status.HTTP_404_NOT_FOUND)
#         try:
#             serializer = self.serializer_class(data=request.data)
#             if not serializer.is_valid():
#                 data_errors = {}
#                 data_message = str("")
#                 for P, M in serializer.errors.items():
#                     data_message += P + ": " + M[0].replace(".", "") + ", "
#                 data_errors["detail"] = data_message
#                 return Response(data_errors, status=status.HTTP_400_BAD_REQUEST)
#             document = serializer.save()
#             try:
#                 company = Company.objects.get(user=current_user)
#             except Profile.DoesNotExist:
#                 return Response(status=status.HTTP_404_NOT_FOUND)
#             profile.save(document_id=document)
#             return Response(
#                 {"detail": _("Document successfully uploaded")},
#                 status=status.HTTP_200_OK,
#             )
#         except Exception as e:
#             return Response({"detail": str(e)}, status=status.HTTP_400_BAD_REQUEST)
