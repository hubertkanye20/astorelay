import uuid
from django.db import models
from django.utils.translation import gettext_lazy as _
from django.contrib.auth import get_user_model
from rest_framework.response import Response
from rest_framework.permissions import *
from django.shortcuts import get_object_or_404
from django.utils.translation import ugettext_lazy as _
from rest_framework.permissions import *
from django.core.exceptions import ObjectDoesNotExist
from rest_framework import status
from rest_framework import generics
from rest_framework.response import Response
from companies.serializers.legal_form import *
from companies.models.legal_form import LegalForm
from rest_framework.permissions import *
from django.utils.translation import ugettext_lazy as _
from rest_framework.permissions import *
from django.core.exceptions import ObjectDoesNotExist
from rest_framework import generics, status
from astorrelay.core.custom_pagination import ResultsSetPagination
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import status, filters


class LegalFormAddView(generics.ListCreateAPIView):

    queryset = LegalForm.objects.all()
    serializer_class = LegalFormAddSerializer
    http_method_names = ["get", "post"]
    filter_backends = [
        filters.SearchFilter,
        DjangoFilterBackend,
        filters.OrderingFilter,
    ]
    filterset_fields = {
        "name": ["exact"],
        "created_at": ["gte", "lte", "exact", "gt", "lt"],
    }
    search_fields = ["^name"]
    ordering_fields = ["created_at", "modified_at"]

    def post(self, request, *args, **kwargs):
        """
        This endpoint lets user add legalform
        """
        serializer = LegalFormAddSerializer(data=request.data)
        try:
            if not serializer.is_valid():
                data_errors = {}
                data_message = str("")
                for P, M in serializer.errors.items():
                    data_message += P + ": " + M[0].replace(".", "") + ", "
                data_errors["detail"] = data_message
                return Response(data_errors, status=status.HTTP_400_BAD_REQUEST)
            legalform = LegalForm.objects.filter(name=request.data["name"])
            if legalform.exists():
                return Response(
                    {"detail": _("A LegalForm with the same type_name already exist")},
                    status=status.HTTP_400_BAD_REQUEST,
                )
            legalform = serializer.save()
            return Response(
                {"detail": _("LegalForm successfully added")},
                status=status.HTTP_200_OK,
            )
        except Exception as e:
            return Response({"detail": str(e)}, status=status.HTTP_400_BAD_REQUEST)

    def get(self, request, *args, **kwargs):
        """
        This endpoint retruns a list of legal forms
        """
        paginator = ResultsSetPagination()
        paginator.page_size = 20
        queryset = self.filter_queryset(self.get_queryset())
        result_page = paginator.paginate_queryset(self.queryset, request)
        serializer = self.serializer_class(result_page, many=True)
        self._paginator = paginator
        return paginator.get_paginated_response(serializer.data)


class LegalFormView(generics.RetrieveUpdateDestroyAPIView):

    queryset = LegalForm.objects.all()
    serializer_class = LegalFormEditSerializer
    lookup_field = "pk"
    http_method_names = ["get", "put", "delete"]

    def get(self, request, *args, **kwargs):
        """
        This endpoint displays legalform details
        """
        try:
            legalform_viewed = self.queryset.get(id=kwargs["pk"])
            serializer = LegalFormViewSerializer(
                legalform_viewed, context={"request": request}
            )
            data = serializer.data
            return Response(data)
        except LegalForm.DoesNotExist:
            return Response({"detail": _("This legalform isn't registered")})

    def put(self, request, *args, **kwargs):
        """
        This endpoint lets user edit legalform
        """
        try:
            legalform = self.queryset.get(id=kwargs["pk"])
            partial = kwargs.pop("partial", True)
            serializer = LegalFormEditSerializer(
                legalform, data=request.data, partial=partial
            )
            if not serializer.is_valid():
                data_errors = {}
                data_message = str("")
                for P, M in serializer.errors.items():
                    data_message += P + ": " + M[0].replace(".", "") + ", "
                data_errors["detail"] = data_message
                return Response(data_errors, status=status.HTTP_400_BAD_REQUEST)
            serializer.save()
            return Response(serializer.data)
        except LegalForm.DoesNotExist:
            return Response({"detail": _("This legalform isn't registered")})

    def delete(self, request, *args, **kwargs):
        """
        This endpoint lets user delete legalform
        """
        try:
            legalform = self.queryset.get(id=kwargs["pk"])
        except ObjectDoesNotExist:
            return Response(
                {"detail": _("This legalform does not exist")},
                status=status.HTTP_400_BAD_REQUEST,
            )
        legalform.delete()
        return Response(
            {"detail": _("LegalForm successfully removed")},
            status=status.HTTP_200_OK,
        )
