from django.contrib import admin
from companies.models.company import Company
from companies.models.legal_form import LegalForm
from companies.models.document import Document

class CompanyAdmin(admin.ModelAdmin):
    list_display = (
        "id",
        "company_name",
        "sponsor_name",
        "phone",
        "activity",
        "address",
    )
    search_fields = ["name"]
    list_per_page = 50
    list_filter = [
        "company_name",
        "sponsor_name",
        "activity",
    ]


class DocumentAdmin(admin.ModelAdmin):
    list_display = (
        "id",
        "document",
        "created_at",
    )
    list_per_page = 50
    list_filter = [
        "created_at",
    ]

class LegalFormAdmin(admin.ModelAdmin):
    list_display = (
        "id",
        "name",
        "created_at",
    )
    search_fields = ["name"]
    list_per_page = 50
    list_filter = [
        "is_active",
        "created_at",
    ]

admin.site.register(LegalForm, LegalFormAdmin)
admin.site.register(Document, DocumentAdmin)
admin.site.register(Company, CompanyAdmin)
