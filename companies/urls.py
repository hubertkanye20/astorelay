from django.urls import path, re_path
from django.urls import path
from companies.views import company


urlpatterns = [
    path("", company.CompanyAddView.as_view(), name="company_add"),
    re_path(
        r"^company/(?P<pk>[a-z0-9\-]+)/$",
        company.CompanyView.as_view(),
        name="company_detail",
    ),
]
