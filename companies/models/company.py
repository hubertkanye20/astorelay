import uuid
import os
from django.db import models
from django.utils import timezone
from django.utils.translation import gettext_lazy as _
from locations.models.country import Country
from companies.models.legal_form import LegalForm
from companies.models.document import Document
from slugify import slugify
from locations.models.city import City
from locations.models.country import Country
from locations.models.province import Province
from  companies.utils.generate_ref_code import generate_code

GENDER_CHOICES = (
    ("MALE", _("Male")),
    ("FEMALE", _("Female")),
    ("UNKNOWN", _("Unknown")),
)

IDENTITY_CARD_CHOICES = (
    ("ID CARD", _("Identity Card")),
    ("PASSPORT", _("Passport")),
)

def get_upload_file_content(instance, filename):
    ext = filename.split(".")[-1]
    title = slugify(instance.title + "astorrelay")
    filename = "%s-%s.%s" % (title, uuid.uuid4(), ext)
    return os.path.join("companies/files/", filename)


def upload_avatar_to(instance, filename):
    ext = filename.split(".")[-1]
    filename = "%s.%s" % (uuid.uuid4(), ext)
    return "/".join(["images", str("avatar").lower(), filename])


class Company(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)

    user_id = models.CharField(_("Owner"), null=True, blank=True,max_length=50
    )
    address = models.CharField(_("Address"), blank=True, null=True, max_length=50)
    company_name = models.CharField(
        _("Company Name"), blank=True, null=True, max_length=255
    )
    sponsor_name = models.CharField(
        _("Sponsor Name"), blank=True, null=True, max_length=255
    )

    phone = models.CharField(_("Phone"), max_length=25, blank=True, null=True)

    activity = models.CharField(_("Activity"), blank=True, null=True, max_length=255)
    document_id = models.ForeignKey(
        Document,
        related_name="docs",
        verbose_name=_("Document"),
        on_delete=models.CASCADE,
    )

    city_id = models.ForeignKey(
        City,
        null=True,
        blank=True,
        related_name="c_cities",
        on_delete=models.CASCADE,
    )

    province_id = models.ForeignKey(
        Province,
        null=True,
        blank=True,
        related_name="c_provinces",
        on_delete=models.CASCADE,
    )
    country_id = models.ForeignKey(
        Country,
        blank=True,
        null=True,
        related_name="c_countries",
        on_delete=models.CASCADE,
    )
    legal_form = models.ForeignKey(
        LegalForm,
        related_name="legal_forms",
        blank=True,
        null=True,
        on_delete=models.CASCADE,
    )

    logo = models.ImageField(
        _("avatar"), upload_to=upload_avatar_to, blank=True, null=True
    )
    reference_code = models.CharField(
        verbose_name=_("Reference Code"), max_length=50, blank=True, null=True
    )
    created_at = models.DateTimeField(_("Created at"), auto_now_add=True)
    modified_at = models.DateTimeField(_("Modified at"), auto_now=True)

    class Meta:
        ordering = ("created_at",)
        verbose_name = _("Company")
        verbose_name_plural = _("Companies")

    def __str__(self):
        return str(self.company_name)

    def save(self, *args, **kwargs):
        if self.reference_code == "" or self.reference_code is None:
            code = generate_code()
            self.reference_code = code
        super(Company, self).save(*args, **kwargs)
