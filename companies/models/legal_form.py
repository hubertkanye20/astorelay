import uuid
from django.db import models
from django.utils.translation import gettext_lazy as _


class LegalForm(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    name = models.CharField(verbose_name=_("Name"), max_length=50)
    full_name = models.CharField(
        verbose_name=_("Full name"), max_length=100, blank=True, null=True
    )
    is_active = models.BooleanField(verbose_name=_("Active"), default=True)
    created_at = models.DateTimeField(verbose_name=_("Created at"), auto_now_add=True)
    modified_at = models.DateTimeField(verbose_name=_("Modified at"), auto_now=True)

    class Meta:
        ordering = ("-created_at",)
        verbose_name = _("Legal Form")
        verbose_name_plural = _("Legal Forms")

    def __str__(self):
        return self.name
