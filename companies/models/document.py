import uuid
import os
from django.db import models
from slugify import slugify
from django.conf import settings
from django.dispatch import receiver
from django.utils.translation import ugettext_lazy as _
from django.db.models.signals import post_save, post_delete


def get_upload_file_document(instance, filename):
    ext = filename.split(".")[-1]
    title = slugify("scanned-card-id")
    filename = "%s-%s.%s" % (title, uuid.uuid4(), ext)
    return os.path.join("companies/documents/", filename)


class Document(models.Model):
    """
    A class based model for storing Docs Identification
    """

    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    document = models.FileField(
        verbose_name=_("ID Card Document"),
        upload_to=get_upload_file_document,
        blank=True,
        null=True,
    )
    is_active = models.BooleanField(verbose_name=_("Active"), default=True)
    created_at = models.DateTimeField(verbose_name=_("Created at"), auto_now_add=True)

    def __str__(self):
        return str(self.id)

    class Meta:
        ordering = ("-created_at",)
        verbose_name = _("Document")
        verbose_name_plural = _("Documents")

    def __unicode__(self):
        return str(self.id)
